/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
import '../css/app.css';
import getNiceMessage from './get_nice_message';
import $ from 'jquery';
import 'bootstrap'; // adds functions to jQuery
import LuckyNumber from './lucky_number';
// uncomment to support legacy code.
global.$ = $;
global.LuckyNumber = LuckyNumber;

console.log(getNiceMessage(5));
