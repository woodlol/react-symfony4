export default class LuckyNumber {
    constructor(container) {
        this.container = $(container);
    }

    initEvents() {
        this.container.on('click', () => {
            this.container.css('color', LuckyNumber.getRandomColor());
        });
    }

    static getRandomColor() {
        const letters = '0123456789ABCDEF';

        let color = '#';

        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }

        return color;
    }
}
