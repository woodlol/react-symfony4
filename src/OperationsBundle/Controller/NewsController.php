<?php declare(strict_types=1);

namespace App\OperationsBundle\Controller;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Repository\ArticleRepository;
use App\CoreBundle\Response\Json\SuccessJsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news", name="app_operations_news_")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param ArticleRepository $articleRepository
     *
     * @return Response
     */
    public function main(ArticleRepository $articleRepository): Response
    {
        /** @var Article[] $articles */
        $articles = $articleRepository->findAll();

        return $this->render('@AppOperations/News/main.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/view/{slug}", name="show")
     *
     * @param Article $article
     *
     * @return Response
     */
    public function show(Article $article): Response
    {
        return $this->render('@AppOperations/News/article.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{slug}/heart", name="toggle_heart", methods={"POST"})
     *
     * @param Article                $article
     * @param LoggerInterface        $logger
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function toggleArticleHeart(
        Article $article,
        LoggerInterface $logger,
        EntityManagerInterface $em
    ): JsonResponse {
        $article->incrementHeartCount();

        $em->flush();

        $logger->info('Article is being hearted!');

        return new SuccessJsonResponse('complited', ['hearts' => $article->getHeartCount()]);
    }
}
