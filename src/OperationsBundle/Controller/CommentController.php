<?php declare(strict_types=1);

namespace App\OperationsBundle\Controller;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Comment;
use App\CoreBundle\Entity\User;
use App\CoreBundle\Response\Json\ErrorJsonResponse;
use App\CoreBundle\Response\Json\HtmlJsonResponse;
use App\CoreBundle\Response\Json\SuccessJsonResponse;
use App\OperationsBundle\Component\CommentComponent;
use App\OperationsBundle\DTO\CommentWidget\CommentInputDataDTO;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/comments", name="app_operations_comments_")
 */
class CommentController extends AbstractController
{

    /**
     * @Route("/main/{id}", name="main")
     *
     * @param Article       $article
     * @param User|UserInterface $user
     *
     * @return HtmlJsonResponse
     */
    public function main(Article $article, UserInterface $user): JsonResponse
    {
        return new HtmlJsonResponse($this->renderView('@AppOperations/News/comments.html.twig', [
            'comments' => $article->getCommentsForUser($user->isRootAccess()),
        ]));
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request          $request
     * @param UserInterface    $user
     * @param CommentComponent $component
     *
     * @return JsonResponse
     */
    public function add(Request $request, UserInterface $user, CommentComponent $component): JsonResponse
    {
        try {
            $data = new CommentInputDataDTO(
                $user->getUsername(),
                $request->get('content'),
                (int) $request->get('articleId')
            );

            $comment = $component->saveComment($data);

            return new HtmlJsonResponse($this->renderView('@AppOperations/News/Components/comment_block.html.twig', [
                'comment' => $comment
            ]));
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }

    /**
     * @Route("/remove/{id}", name="remove", defaults={"isRemove": "1"})
     * @Route("/restore/{id}", name="restore", defaults={"isRemove": "0"})
     *
     * @param Comment                $comment
     * @param bool                   $isRemove
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function change(Comment $comment, bool $isRemove, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $comment->setIsDeleted($isRemove);

            $entityManager->persist($comment);
            $entityManager->flush();

            return new SuccessJsonResponse('Comment has been ' . $isRemove ? 'removed.' : 'restore.', [
                'isRemove' => $isRemove,
            ]);
        } catch (\Throwable $exception) {
            return new ErrorJsonResponse($exception);
        }
    }
}
