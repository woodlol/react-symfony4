<?php declare(strict_types=1);

namespace App\OperationsBundle\Controller;

use App\CoreBundle\Components\Common;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lucky", name="app_lucky_")
 */
class LuckyController extends AbstractController
{
    /**
     * @Route("/number/{max}", name="number")
     *
     * @param int    $max
     *
     * @param Common $common
     *
     * @return Response
     * @throws \Exception
     */
    public function number(Common $common, int $max = 100): Response
    {
        return $this->render('@AppOperations/LuckyNumber/number.html.twig', [
            'number' => $common->getRandValue($max),
        ]);
    }
}
