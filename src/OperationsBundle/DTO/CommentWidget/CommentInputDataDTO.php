<?php declare(strict_types=1);

namespace App\OperationsBundle\DTO\CommentWidget;

use Webmozart\Assert\Assert;

class CommentInputDataDTO
{
    /** @var null|string */
    private $userName;

    /** @var null|string */
    private $context;

    /** @var null|int */
    private $articleId;


    /**
     * @param string|null $userName
     * @param string|null $context
     * @param int|null    $articleId
     *
     * @throws \Exception
     */
    public function __construct(?string $userName, ?string $context, ?int $articleId)
    {
        $this->userName  = $userName;
        $this->context   = $context;
        $this->articleId = $articleId;

        Assert::notEmpty($userName);
        Assert::notEmpty($articleId);
        Assert::notEmpty(\trim($context), 'fill some text, bro)');
    }


    /**
     * @return string|null
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string|null
     */
    public function getContext(): string
    {
        return $this->context;
    }

    /**
     * @return int|null
     */
    public function getArticleId(): int
    {
        return $this->articleId;
    }
}
