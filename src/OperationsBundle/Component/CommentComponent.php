<?php declare(strict_types=1);

namespace App\OperationsBundle\Component;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Comment;
use App\OperationsBundle\DTO\CommentWidget\CommentInputDataDTO;
use Doctrine\ORM\EntityManagerInterface;

class CommentComponent
{
    /** @var EntityManagerInterface  */
    private $em;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param CommentInputDataDTO $comment
     *
     * @return Comment
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function saveComment(CommentInputDataDTO $comment): Comment
    {
        /** @var Article $article */
        $article = $this->em->getRepository(Article::class)->findOneByOrFail(['id' => $comment->getArticleId()]);

        $comment = Comment::create($comment->getUserName(), $comment->getContext());
        $comment->setArticle($article);

        $this->em->persist($comment);
        $this->em->flush();

        return $comment;
    }
}
