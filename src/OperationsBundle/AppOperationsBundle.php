<?php declare(strict_types=1);

namespace App\OperationsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppOperationsBundle extends Bundle
{
}
