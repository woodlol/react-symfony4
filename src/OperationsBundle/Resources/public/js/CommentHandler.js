class CommentHandler {
    constructor(container) {
        this.form = $(container);
        this.btn = this.form.find('button[type=submit]');
        this.counter = $('.js-counter-comment');

        this.form.on('submit', (event) => {
            event.preventDefault();

            let currentValue = Number(this.counter.html());

            $.ajax({
                url: this.form.attr('action'),
                method: this.form.attr('method'),
                data: new FormData(this.form.get(0)),
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: () => {
                    CommentHandler.lockBtn(this.btn, true);
                },
                complete: () => {
                    CommentHandler.lockBtn(this.btn, false);
                },
                success: (res) => {
                    if (res.status !== 'success') {
                        alert(res.message)
                    } else {
                        this.form.find('#comment_form_content').val('');
                        this.counter.html(++currentValue);

                        this.loadWidget();
                    }
                }
            });
        });
    }

    setContainer(container) {
        this.container = $(container);
    }

    loadWidget() {
        const href = this.container.data('href');

        $.ajax({
            url: href,
            beforeSend: () => {
              this.container.html('Loading....');
            },
        }).done((res) => {
            this.container.html(res.html);
            this.initEvents();
        });
    }

    initEvents() {
        this.btnRemoveBtn = $('.js-comment-btn');

        this.btnRemoveBtn.on('click', (event) => {
            const
                btn = $(event.currentTarget),
                href = btn.data('href');

            let currentValue = Number(this.counter.html());

            $.ajax({
                url: href,
                beforeSend: () => {
                    CommentHandler.lockBtn(btn, true);
                },
                complete: () => {
                    CommentHandler.lockBtn(btn, false);
                },
                success: (res) => {
                    if (res.status !== 'success') {
                        alert(res.message)
                    } else {
                        let newValue = res.isRemove ? --currentValue : ++currentValue;
                        this.counter.html(newValue);
                        this.loadWidget();
                    }
                }
            })


        });
    }

    static lockBtn(btn, disabled) {
        btn.prop('disabled', disabled);
    }
}
