<?php declare(strict_types=1);

namespace App\CoreBundle\Response\Json;

class NoticeJsonResponse extends AbstractJsonResponse
{
    protected static $stringStatus = self::STATUS_NOTICE;
}
