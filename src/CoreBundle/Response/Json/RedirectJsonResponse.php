<?php declare(strict_types=1);

namespace App\CoreBundle\Response\Json;

class RedirectJsonResponse extends AbstractJsonResponse
{
    protected static $stringStatus = self::STATUS_REDIRECT;

    /**
     * @param string $url
     * @param array  $data
     * @param string $message
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($url, array $data = [], $message = '', $status = 200, array $headers = [])
    {
        $data['url'] = $url;

        parent::__construct($message, $data, $status, $headers);
    }
}
