<?php declare(strict_types=1);

namespace App\CoreBundle\Response\Json;

class FileContentJsonResponse extends AbstractJsonResponse
{
    /**
     * @param string $filename
     * @param string $data
     * @param string $message
     */
    public function __construct(string $filename, string $data, string $message = '')
    {
        parent::__construct($message, [
            'filename' => $filename,
            'data'     => \utf8_encode($data),
        ]);
    }
}
