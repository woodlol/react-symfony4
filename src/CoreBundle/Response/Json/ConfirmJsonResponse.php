<?php declare(strict_types=1);

namespace App\CoreBundle\Response\Json;

class ConfirmJsonResponse extends AbstractJsonResponse
{
    protected static $stringStatus = self::STATUS_CONFIRM;
}
