<?php declare(strict_types=1);

namespace App\CoreBundle\Repository;

use Doctrine\Common\Collections\Criteria;

class CommentRepository extends DefaultEntityRepository
{
    public static function createNonDeletedCriteria(): Criteria
    {
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq('isDeleted', false))
            ->orderBy(['createdAt' => 'DESC'])
            ;
    }
}
