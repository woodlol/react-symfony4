<?php declare(strict_types=1);

namespace App\CoreBundle\Repository;

use App\CoreBundle\Components\Doctrine\Hydrators\ColumnHydrator;
use App\CoreBundle\Components\Doctrine\Hydrators\CustomHydrators;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

abstract class DefaultEntityRepository extends EntityRepository implements ServiceEntityRepositoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, $entityManager->getClassMetadata($this->getEntityClass()));

        $entityManager
            ->getConfiguration()
            ->addCustomHydrationMode((string) CustomHydrators::COLUMN_HYDRATOR, ColumnHydrator::class);
    }

    /**
     * Replace full repository class name to entity name.
     *
     * If result does not match to entity it can be set manually in this method:
     *
     * @return string
     */
    protected function getEntityClass(): string
    {
        return \mb_substr(\str_replace('Repository', 'Entity', static::class), 0, -6);
    }

    /**
     * @param string     $alias
     * @param array      $criteria
     * @param string     $select
     * @param null|array $orderBy
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return null|object
     */
    public function partialFindOneBy(string $alias, array $criteria, string $select, array $orderBy = [])
    {
        $builder = $this->createQueryBuilder($alias);
        foreach ($criteria as $field => $value) {
            $builder
                ->andWhere("{$alias}.{$field} = :{$field}_")
                ->setParameter("{$field}_", $value);
        }
        foreach ($orderBy as $field => $sortOrder) {
            $builder
                ->orderBy("{$alias}.{$field}", $sortOrder ? $sortOrder : 'ASC');
        }

        return $builder
            ->select($select)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param int         $id
     * @param null|string $message
     *
     * @throws EntityNotFoundException
     *
     * @return object
     */
    public function findOrFail($id, $message = null)
    {
        $result = $this->find($id);

        if (null === $result) {
            throw new EntityNotFoundException($message ?? $this->_entityName . ' not found!');
        }

        return $result;
    }

    /**
     * @param array       $criteria
     * @param null|array  $orderBy
     * @param null|string $message
     *
     * @throws EntityNotFoundException
     *
     * @return object
     */
    public function findOneByOrFail(array $criteria, array $orderBy = null, $message = null)
    {
        $result = parent::findOneBy($criteria, $orderBy);

        if (null === $result) {
            throw new EntityNotFoundException($message ?? $this->_entityName . ' not found!');
        }

        return $result;
    }

    /**
     * @param array       $criteria
     * @param null|array  $orderBy
     * @param null|int    $limit
     * @param null|int    $offset
     * @param null|string $message
     *
     * @throws EntityNotFoundException
     *
     * @return array
     */
    public function findByOrFail(array $criteria, array $orderBy = null, $limit = null, $offset = null, $message = null)
    {
        $result = parent::findBy($criteria, $orderBy, $limit, $offset);

        if (empty($result)) {
            throw new EntityNotFoundException($message ?? 'Nothing was found!');
        }

        return $result;
    }

    /**
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function updateQuery($alias): QueryBuilder
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->update($this->getEntityName(), $alias);
    }

    /**
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function deleteQuery($alias): QueryBuilder
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->delete($this->getEntityName(), $alias);
    }
}
