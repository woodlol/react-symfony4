<?php declare(strict_types=1);

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="securities")
 */
class Security
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToOne(targetEntity="App\CoreBundle\Entity\Enclosure", inversedBy="securities")
     */
    private $enclosure;

    /**
     * @param string    $name
     * @param bool      $isActive
     * @param Enclosure $enclosure
     */
    public function __construct(string $name, bool $isActive, Enclosure $enclosure)
    {
        $this->name      = $name;
        $this->isActive  = $isActive;
        $this->enclosure = $enclosure;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }
}
