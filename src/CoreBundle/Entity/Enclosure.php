<?php declare(strict_types=1);

namespace App\CoreBundle\Entity;

use App\CoreBundle\Exception\DinosaursAreRunningRampantException;
use App\CoreBundle\Exception\NotABuffetException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\CoreBundle\Repository\EnclosureRepository")
 * @ORM\Table(name="enclosures")
 */
class Enclosure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @var Collection
     * @ORM\OneToMany(targetEntity="App\CoreBundle\Entity\Dinosaur", mappedBy="enclosure", cascade={"persist"})
     */
    private $dinosaurs;

    /**
     * @var Collection|Security[]
     * @ORM\OneToMany(targetEntity="App\CoreBundle\Entity\Security", mappedBy="enclosure", cascade={"persist"})
     */
    private $securities;

    /**
     * @param bool $withBasicSecurity
     */
    public function __construct(bool $withBasicSecurity = false)
    {
        $this->dinosaurs  = new ArrayCollection();
        $this->securities = new ArrayCollection();

        if ($withBasicSecurity) {
            $this->addSecurity(new Security('Fence', true, $this));
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getDinosaurs(): Collection
    {
        return $this->dinosaurs;
    }

    /**
     * @param Dinosaur $dinosaur
     *
     * @throws DinosaursAreRunningRampantException
     * @throws NotABuffetException
     */
    public function addDinosaur(Dinosaur $dinosaur)
    {
        if (!$this->isSecurityActive()) {
            throw new DinosaursAreRunningRampantException('Are you craaazy?!?');
        }

        if (!$this->canAddDinosaur($dinosaur)) {
            throw new NotABuffetException();
        }

        $this->dinosaurs[] = $dinosaur;
    }

    /**
     * @param Security $security
     */
    public function addSecurity(Security $security)
    {
        $this->securities[] = $security;
    }

    /**
     * @param Dinosaur $dinosaur
     *
     * @return bool
     */
    private function canAddDinosaur(Dinosaur $dinosaur): bool
    {
        return count($this->dinosaurs) === 0 || $this->dinosaurs->first()->isCarnivorous() === $dinosaur->isCarnivorous();
    }

    /**
     * @return bool
     */
    public function isSecurityActive(): bool
    {
        foreach ($this->securities as $security) {
            if ($security->getIsActive()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Collection
     */
    public function getSecurities(): Collection
    {
        return $this->securities;
    }

    /**
     * @return int
     */
    public function getDinosaurCount(): int
    {
        return $this->dinosaurs->count();
    }
}
