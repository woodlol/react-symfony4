<?php declare(strict_types=1);

namespace App\CoreBundle\Factory;

use App\CoreBundle\Entity\Dinosaur;
use App\CoreBundle\Components\DinosaurLengthDeterminator;

class DinosaurFactory
{
    /** @var DinosaurLengthDeterminator  */
    private $lengthDeterminator;

    /**
     * @param DinosaurLengthDeterminator $lengthDeterminator
     */
    public function __construct(DinosaurLengthDeterminator $lengthDeterminator)
    {
        $this->lengthDeterminator = $lengthDeterminator;
    }

    /**
     * @param int $length
     *
     * @return Dinosaur
     */
    public function growVelociraptor(int $length): Dinosaur
    {
        return $this->createDinosaur('Velociraptor', true, $length);
    }

    /**
     * @param string $specification
     *
     * @return Dinosaur
     * @throws \Exception
     */
    public function growFromSpecification(string $specification): Dinosaur
    {
        // defaults
        $codeName      = 'InG-' . \rand(1, 99999);
        $length        = $this->lengthDeterminator->getLengthFromSpecification($specification);
        $isCarnivorous = false;

        if (false !== \stripos($specification, 'carnivorous')) {
            $isCarnivorous = true;
        }

        $dinosaur = $this->createDinosaur($codeName, $isCarnivorous, $length);

        return $dinosaur;
    }

    /**
     * @param string $genus
     * @param bool   $isCarnivorous
     * @param int    $length
     *
     * @return Dinosaur
     */
    private function createDinosaur(string $genus, bool $isCarnivorous, int $length): Dinosaur
    {
        return (new Dinosaur($genus, $isCarnivorous))->setLength($length);
    }
}
