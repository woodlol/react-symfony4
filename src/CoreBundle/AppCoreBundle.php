<?php declare(strict_types=1);

namespace App\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppCoreBundle extends Bundle
{
}
