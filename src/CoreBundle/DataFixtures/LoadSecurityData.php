<?php declare(strict_types=1);

namespace App\CoreBundle\DataFixtures;

use App\CoreBundle\Entity\Enclosure;
use App\CoreBundle\Entity\Security;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSecurityData extends Fixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $herbivorousEnclosure = $this->getReference('herbivorous-enclosure');

        $this->addSecurity($herbivorousEnclosure, 'Fence', true);

        $carnivorousEnclosure = $this->getReference('carnivorous-enclosure');

        $this->addSecurity($carnivorousEnclosure, 'Electric fence', false);
        $this->addSecurity($carnivorousEnclosure, 'Guard tower', false);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 2;
    }

    /**
     * @param Enclosure $enclosure
     * @param string    $name
     * @param bool      $isActive
     */
    private function addSecurity(Enclosure $enclosure, string $name, bool $isActive): void
    {
        $enclosure->addSecurity(new Security($name, $isActive, $enclosure));
    }
}
