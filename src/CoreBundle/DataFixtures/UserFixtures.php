<?php declare(strict_types=1);

namespace App\CoreBundle\DataFixtures;

use App\CoreBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{
    /** @var UserPasswordEncoderInterface  */
    private $passwordEncoder;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(User::class, 100, function(User $user) {
            $user->setName($this->faker->userName);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $this->faker->password));
        });

        $manager->persist($this->getDefaultRootUser());
        $manager->flush();
    }

    /**
     * @return User
     */
    private function getDefaultRootUser(): User
    {
        $user = new User();

        return $user
            ->setName(User::DEFAULT_ROOT_NAME)
            ->setRoles([User::ROLE_ADMIN])
            ->setPassword($this->passwordEncoder->encodePassword($user, User::DEFAULT_PASSWORD));
    }
}
