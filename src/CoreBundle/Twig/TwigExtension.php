<?php declare(strict_types=1);

namespace App\CoreBundle\Twig;

use App\CoreBundle\Components\MarkdownHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigExtension extends AbstractExtension
{
    /** @var MarkdownHelper  */
    private $markdownHelper;

    /**
     * @param MarkdownHelper $container
     */
    public function __construct(MarkdownHelper $container)
    {
        $this->markdownHelper = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('default_date_format', [$this, 'dateFormatFilter']),
            new TwigFilter('cached_markdown', [$this, 'processMarkdown'], ['is_safe' => ['html']]),
        ];
    }


    /**
     * @param null|\DateTime $dateTime
     * @param $format
     * @param string $returnIfNull
     *
     * @return string
     */
    public function dateFormatFilter(\DateTime $dateTime = null, $format = 'd-m-Y H:i:s', $returnIfNull = '-'): string
    {
        if (null === $dateTime) {
            return $returnIfNull;
        }

        return $dateTime->format($format);
    }

    /**
     * @param $value
     *
     * @return string
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function processMarkdown($value): string
    {
        return $this->markdownHelper->parse($value);
    }
}
