<?php declare(strict_types=1);

namespace App\CoreBundle\Controller;

use App\CoreBundle\Entity\Enclosure;
use App\CoreBundle\Factory\DinosaurFactory;
use App\CoreBundle\Repository\EnclosureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/default", name="app_core_default_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param EnclosureRepository $enclosureRepository
     *
     * @return Response
     */
    public function main(EnclosureRepository $enclosureRepository): Response
    {
        return $this->render('@AppCore/Default/index.html.twig', [
            'enclosures' => $enclosureRepository->findAll(),
        ]);
    }

    /**
     * @Route("/grow", name="grow_dinosaur", methods={"POST"})
     *
     * @param Request         $request
     * @param DinosaurFactory $dinosaurFactory
     *
     * @throws \Exception
     *
     * @return RedirectResponse
     */
    public function growAction(Request $request, DinosaurFactory $dinosaurFactory): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();

        $enclosure = $manager->getRepository(Enclosure::class)
            ->find($request->request->get('enclosure'));

        $specification = $request->request->get('specification');
        $dinosaur = $dinosaurFactory->growFromSpecification($specification);

        $dinosaur->setEnclosure($enclosure);
        $enclosure->addDinosaur($dinosaur);

        $manager->flush();

        $this->addFlash('success', sprintf(
            'Grew a %s in enclosure #%d',
            mb_strtolower($specification),
            $enclosure->getId()
        ));

        return $this->redirectToRoute('app_core_default_main');
    }
}
