<?php declare(strict_types=1);

namespace App\CoreBundle\Controller;

use App\CoreBundle\Components\SecurityComponentHandler;
use App\CoreBundle\Entity\User;
use App\CoreBundle\Form\UserRegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/security", name="app_security_")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@AppDesign/LoginPage/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route("/register", name="register")
     *
     * @param Request                  $request
     * @param SecurityComponentHandler $securityComponentHandler
     *
     * @return Response
     */
    public function register(
        Request $request,
        SecurityComponentHandler $securityComponentHandler
    ): Response {
        $form = $this->createForm(UserRegistrationFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            return $securityComponentHandler->registerUser($request, $user);
        }

        return $this->render('@AppDesign/Register/main.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/logout", name="logout")
     *
     * @throws \Exception
     */
    public function logout(): void
    {
        throw new \Exception('Will be intercepted before getting here');
    }
}
