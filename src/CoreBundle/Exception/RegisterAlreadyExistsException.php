<?php declare(strict_types=1);

namespace App\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

class RegisterAlreadyExistsException extends \Exception
{
    public function __construct($userName = "")
    {
        parent::__construct('Name: '. $userName . ' has already been registered.', Response::HTTP_BAD_REQUEST);
    }
}
