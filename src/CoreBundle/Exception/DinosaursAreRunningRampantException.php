<?php declare(strict_types=1);

namespace App\CoreBundle\Exception;

class DinosaursAreRunningRampantException extends \Exception
{
}
