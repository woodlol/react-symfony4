<?php declare(strict_types=1);

namespace App\CoreBundle\Components;

use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\CacheItem;

class MarkdownHelper
{
    /** @var AdapterInterface  */
    private $cache;

    /** @var MarkdownInterface  */
    private $markdown;

    /** @var LoggerInterface  */
    private $logger;

    /** @var bool  */
    private $isDebug;

    /**
     * @param AdapterInterface  $cache
     * @param MarkdownInterface $markdown
     * @param LoggerInterface   $markdownLogger
     * @param bool              $isDebug
     */
    public function __construct(
        AdapterInterface $cache,
        MarkdownInterface $markdown,
        LoggerInterface $markdownLogger,
        bool $isDebug
    ) {
        $this->cache = $cache;
        $this->markdown = $markdown;
        $this->logger = $markdownLogger;
        $this->isDebug = $isDebug;
    }

    /**
     * @param string $source
     *
     * @return string
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function parse(string $source): string
    {
        if (\stripos($source, 'bacon') !== false) {
            $this->logger->info('They are talking about bacon again!');
        }

        $item = $this->cache->getItem('markdown_' . \md5($source));

        if (!$item instanceof CacheItem) {
            return '';
        }

        // skip caching entirely in debug
        if ($this->isDebug) {
            return $this->markdown->transform($source);
        }

        if (!$item->isHit()) {
            $item->set($this->markdown->transform($source));
            $this->cache->save($item);
        }

        return $item->get();
    }
}
