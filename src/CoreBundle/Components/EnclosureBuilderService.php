<?php declare(strict_types=1);

namespace App\CoreBundle\Components;

use App\CoreBundle\Entity\Enclosure;
use App\CoreBundle\Entity\Security;
use App\CoreBundle\Factory\DinosaurFactory;
use Doctrine\ORM\EntityManagerInterface;

class EnclosureBuilderService
{
    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var DinosaurFactory  */
    private $dinosaurFactory;

    public function __construct(EntityManagerInterface $entityManager, DinosaurFactory $dinosaurFactory)
    {
        $this->entityManager   = $entityManager;
        $this->dinosaurFactory = $dinosaurFactory;
    }

    /**
     * @param int $numberOfSecuritySystems
     * @param int $numberOfDinosaurs
     *
     * @throws \App\CoreBundle\Exception\DinosaursAreRunningRampantException
     * @throws \App\CoreBundle\Exception\NotABuffetException
     *
     * @return Enclosure
     */
    public function buildEnclosure(int $numberOfSecuritySystems = 1, int $numberOfDinosaurs = 3): Enclosure
    {
        $enclosure = new Enclosure();

        $this->addSecuritySystems($numberOfSecuritySystems, $enclosure);
        $this->addDinosaurs($numberOfDinosaurs, $enclosure);

        $this->entityManager->persist($enclosure);
        $this->entityManager->flush();

        return $enclosure;
    }

    /**
     * @param int       $numberOfSecuritySystems
     * @param Enclosure $enclosure
     */
    private function addSecuritySystems(int $numberOfSecuritySystems, Enclosure $enclosure): void
    {
        $securityNames = ['Fence', 'Electric fence', 'Guard tower'];

        for ($i = 0; $i < $numberOfSecuritySystems; $i++) {
            $enclosure->addSecurity(new Security($securityNames[\rand(0, \count($securityNames) - 1)], true, $enclosure));
        }
    }

    /**
     * @param int       $numberOfDinosaurs
     * @param Enclosure $enclosure
     *
     * @throws \App\CoreBundle\Exception\DinosaursAreRunningRampantException
     * @throws \App\CoreBundle\Exception\NotABuffetException
     */
    private function addDinosaurs(int $numberOfDinosaurs, Enclosure $enclosure): void
    {
        for ($i = 0; $i < $numberOfDinosaurs; $i++) {
            $length        = \array_rand(['small', 'large', 'huge']);
            $diet          = \array_rand(['herbivore', 'carnivorous']);
            $specification = "{$length} {$diet} dinosaur";
            $dinosaur      = $this->dinosaurFactory->growFromSpecification($specification);

            $enclosure->addDinosaur($dinosaur);
        }
    }
}
