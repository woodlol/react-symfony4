<?php declare(strict_types=1);

namespace App\CoreBundle\Components\Doctrine\Hydrators;

class CustomHydrators
{
    public const COLUMN_HYDRATOR = 100;
}
