<?php declare(strict_types=1);

namespace App\CoreBundle\Components;

use App\CoreBundle\Entity\User;
use App\CoreBundle\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class SecurityComponentHandler
{
    /** @var UserPasswordEncoderInterface  */
    private $passwordEncoder;

    /** @var GuardAuthenticatorHandler  */
    private $guardHandler;

    /** @var LoginFormAuthenticator  */
    private $formAuthenticator;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler    $guardHandler
     * @param LoginFormAuthenticator       $formAuthenticator
     * @param EntityManagerInterface       $entityManager
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        LoginFormAuthenticator $formAuthenticator,
        EntityManagerInterface $entityManager
    ) {
        $this->passwordEncoder   = $passwordEncoder;
        $this->guardHandler      = $guardHandler;
        $this->formAuthenticator = $formAuthenticator;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param User    $user
     *
     * @return Response
     */
    public function registerUser(Request $request, User $user): Response
    {
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $user->getPassword()
        ));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->guardHandler->authenticateUserAndHandleSuccess($user, $request, $this->formAuthenticator, 'main');
    }
}
