<?php declare(strict_types=1);

namespace App\CoreBundle\Components;

/**
 * Common service for different functions.
 */
class Common
{
    /**
     * @param null $max
     *
     * @return int
     */
    public function getRandValue($max = null): int
    {
        return \rand(0 , $max ?? 100);
    }
}
