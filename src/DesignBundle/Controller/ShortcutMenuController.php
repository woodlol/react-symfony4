<?php declare(strict_types=1);

namespace App\DesignBundle\Controller;

use App\DesignBundle\Component\Menu\Renderer;
use App\DesignBundle\Menu\Builder\MenuBuilder;
use App\DesignBundle\Menu\Site;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ShortcutMenuController extends AbstractController
{
    /**
     * @param Renderer      $renderer
     *
     * @return Response
     */
    public function displayAction(Renderer $renderer): Response
    {
        $fullMenu  = (new Site())->mainMenu(); //get instance of menu
        $shortMenu = new MenuBuilder();

        foreach ($fullMenu->getItems() as $item) {
            $shortMenu->addMenuElement($item);
        }

        $params = $renderer->toParameters($shortMenu);

        return $this->render('@AppDesign/Menu/menu_shortcuts.html.twig', $params);
    }
}
