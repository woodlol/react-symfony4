<?php declare(strict_types=1);

namespace App\DesignBundle\Controller;

use App\CoreBundle\Components\Common;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="app_home_")
 */
class HomePageController extends AbstractController
{
    /**
     * @Route("/main", name="main")
     *
     * @param Common $common
     *
     * @return Response
     */
    public function main(Common $common): Response
    {
        return $this->render('@AppDesign/HomePage/main.html.twig', [
            'url' => $this->generateUrl('app_lucky_number', [
                'max' => $common->getRandValue(),
            ])
        ]);
    }
}
