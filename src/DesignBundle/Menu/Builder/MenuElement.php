<?php declare(strict_types=1);

namespace App\DesignBundle\Menu\Builder;

class MenuElement
{
    /** @var MenuBuilder  */
    private $parent;

    /** @var null|MenuBuilder */
    private $submenu = null;

    /** @var string */
    private $caption;

    /** @var string */
    private $route;

    /** @var array */
    private $routeParams = [];

    /** @var int  */
    private $id;

    /** @var null|string */
    private $aClass;

    /**
     * Creates a new menu option element. Each menu option must be a child of MenuBuilder and must have a unique id
     * within the whole menu tree.
     *
     * @param MenuBuilder $parent
     * @param int         $id
     */
    public function __construct(MenuBuilder $parent, int $id)
    {
        $this->parent = $parent;
        $this->id = $id;
    }


    /**
     * Sets the caption of this menu item
     * Caption is what gets displayed to the user as the link/button text
     * @param string $cap the caption text
     *
     * @return $this return this object for function chaining
     */
    public function caption(string $cap): self
    {
        $this->caption = $cap;

        return $this;
    }

    /**
     * The css class to apply to the <a> element
     *
     * @param null|string $class
     *
     * @return $this
     */
    public function linkclass(?string $class): self
    {
        $this->aClass = $class;

        return $this;
    }

    /**
     * The href link will be dynamically set based on the provided route name
     * Use this when linking to pages that are within your project and have defined routes.
     *
     * @param string  $route name of the route to link to
     * @param array $params if route requires parameters specify them here
     *
     * @return self
     */
    public function route(string $route, array $params = []): self
    {
        $this->route       = $route;
        $this->routeParams = $params;

        return $this;
    }

    /**
     * @return MenuBuilder|null
     */
    public function submenu(): ?MenuBuilder
    {
        if ($this->submenu !== null) {
            throw new \LogicException("Only one sub-menu is allowed per menu element. Called submenu() twice on the same MenuElement");
        }

        $this->submenu = new MenuBuilder();
        $this->submenu->setParent($this);

        return $this->submenu;
    }

    /**
     * @return MenuBuilder
     */
    public function end(): MenuBuilder
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return null|string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * @return bool
     */
    public function hasSubmenu(): bool
    {
        return $this->submenu !== null;
    }

    /**
     * @return MenuBuilder
     */
    public function getSubmenu()
    {
        return $this->submenu;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getLinkClass(): ?string
    {
        return $this->aClass;
    }
}
