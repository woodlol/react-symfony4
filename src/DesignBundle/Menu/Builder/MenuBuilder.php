<?php declare(strict_types=1);

namespace App\DesignBundle\Menu\Builder;

class MenuBuilder
{
    /** @var null|int */
    private $id;

    /** @var  MenuElement */
    private $parent = null;

    /** @var MenuElement[] */
    private $children = array();

    /**
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Adds a new menu element to the menu represented by this builder.
     *
     * @param int $id unique menu id
     *
     * @return MenuElement the newly added element
     */
    public function item(int $id): MenuElement
    {
        $item             = new MenuElement($this, $id);
        $this->children[] = $item;

        return $item;
    }

    /**
     * Marks the end of the menu represented by this builder.
     * If this is a sub-menu the MenuElement containing this sub-menu is returned to allow chaining.
     * If this is a root of the menu, null is returned.
     * @return MenuElement|null
     */
    public function end(): ?MenuElement
    {
        return $this->parent;
    }

    /**
     * Sets the parent of this builder to be the specified element
     * @param MenuElement $e
     */
    public function setParent(MenuElement $e)
    {
        $this->parent = $e;
    }

    /**
     * Returns the id of the menu represented by this builder.
     * Note that if this is a sub-menu or the id was not set, this function will return null.
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Returns all the menu items within the menu represented by this builder
     * @return MenuElement[]
     */
    public function getItems(): array
    {
        return $this->children;
    }

    /**
     * Adds menu element to this menu.
     * Used to build menu from existing menu elements.
     * @param MenuElement $item
     */
    public function addMenuElement(MenuElement &$item)
    {
        $this->children[] = $item;
    }

    /**
     * Finds a MenuElement in this MenuBuilder which has the specified id. Searches this menu and sub-menus.
     *
     * @param int $id the id of the MenuElement to find
     *
     * @return MenuElement|null returns the menu element if found, null otherwise
     */
    public function &findItem($id): ?MenuElement
    {
        $item = null;

        foreach ($this->children as &$child) {
            if ($child->getId() == $id) {
                return $child;
            }
            if ($child->hasSubmenu()) {
                $item =& $child->getSubmenu()->findItem($id);
                if ($item !== null) {
                    return $item;
                }
            }
        }

        return $item;
    }
}
