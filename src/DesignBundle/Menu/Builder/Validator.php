<?php declare(strict_types=1);

namespace App\DesignBundle\Menu\Builder;

/**
 * Used for validating MenuBuilder for consistency.
 */
class Validator
{
    /**
     * Validates the provided MenuBuilder.
     * Checks that no 2 menu items have the same id, as all of them must be unique.
     *
     * @param MenuBuilder $mb the menu to be validated
     * @throws \UnexpectedValueException when duplicate ids are detected.
     */
    public static function validate(MenuBuilder $mb): void
    {
        $items     = $mb->getItems();
        $allIds    = array();
        $duplicate = array();
        $max       = 0;

        self::validateItems($items, $allIds, $duplicate, $max);
        if (!empty($duplicate)) {
            //we have a problem, duplicate id was detected
            $message = PHP_EOL . "Duplicate menu id(s) detected in menu {$mb->getId()}: {" . implode(", ", $duplicate) . "} " . PHP_EOL
                . "Please review and correct the menu class coding. If you added a new menu item, make sure you update "
                . "its id to a number greater than the current maximum." . PHP_EOL .
                "Current maximum id is: $max" . PHP_EOL;
            throw new \UnexpectedValueException($message);
        }
    }

    /**
     * Helper function which recursively goes through each menu and submenu to check for duplicate ids.
     *
     * @param MenuElement[] $items
     * @param array         $allIds
     * @param array         $duplicate
     * @param int           $max
     */
    private static function validateItems($items, &$allIds, &$duplicate, &$max): void
    {
        foreach ($items as &$item) {
            $id = $item->getId();
            if (isset($allIds[$id])) {
                $duplicate[] = $id; //record a duplicate id
            } else {
                $allIds[$id] = true;
            }
            //record maximum id to help out developer
            $max = max($max, (int)$id);
            if ($item->hasSubmenu()) {
                self::validateItems($item->getSubmenu()->getItems(), $allIds, $duplicate, $max);
            }
        }
    }
}
