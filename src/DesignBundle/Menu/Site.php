<?php declare(strict_types=1);

namespace App\DesignBundle\Menu;

use App\DesignBundle\Menu\Builder\MenuBuilder;

class Site
{
    /**
     * @return MenuBuilder
     */
    public function mainMenu(): MenuBuilder
    {
        /** Max Id Item 7 */
        $menu = new MenuBuilder('draggable-main-menu');
        $this->addOperations($menu);
        $this->addNews($menu);

        return $menu;
    }

    /**
     * @param MenuBuilder $menu
     */
    private function addOperations(MenuBuilder $menu): void
    {
        $menu
            ->item(1)->caption('Menu item 1 (Operations)')->linkclass('glyphicon-hdd')
                ->submenu()
                ->item(2)->caption('Lucky Number')->route('app_lucky_number')->end()
                ->item(3)->caption('Item 3')->end()
                ->item(4)->caption('Sub Menu 2')
                    ->submenu()
                    ->item(5)->caption('Item 4')->end()
            ->end();
    }

    /**
     * @param MenuBuilder $menu
     */
    private function addNews(MenuBuilder $menu): void
    {
        $menu
            ->item(6)->caption('Menu item 2')->linkclass('glyphicon glyphicon-list-alt')
            ->submenu()
                ->item(7)->caption('Random Item')->end()
                ->item(8)->caption('NEWS!')->route('app_operations_news_main')->end()
            ->end();
    }
}
