<?php declare(strict_types=1);

namespace App\DesignBundle\Component\Menu;

use App\DesignBundle\Menu\Builder\MenuBuilder;
use App\DesignBundle\Menu\Builder\MenuElement;
use App\DesignBundle\Menu\Builder\Validator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * Used by twig extension to render menus based on MenuBuilder class
 */
class Renderer
{
    /** @var KernelInterface  */
    private $kernel;

    /** @var string  */
    private $defaultTemplate;

    /** @var array  */
    private $builderCache = [];

    /** @var LoggerInterface */
    private $logger;

    /** @var Environment */
    private $twig;

    /** @var RouterInterface */
    private $router;

    /**
     * Creates new instance of Renderer.
     * KernelInterface is used for retrieval of various services and bundles.
     * Requires default twig template, which will be used to render the menu. The template must be specified using
     * standard syntax: bundleName/ViewFolder/templatefile.html.twig
     *
     * @param KernelInterface $kernel
     * @param LoggerInterface $logger
     * @param Environment     $twig
     * @param RouterInterface $router
     * @param string          $defaultTemplate
     */
    public function __construct(
        KernelInterface $kernel,
        LoggerInterface $logger,
        Environment $twig,
        RouterInterface $router,
        string $defaultTemplate
    ) {
        $this->kernel          = $kernel;
        $this->logger          = $logger;
        $this->twig            = $twig;
        $this->router          = $router;
        $this->defaultTemplate = $defaultTemplate;
    }

    /**
     * Here is where the rendering occurs. This function combines the menu build in MenuBuilder with a twig template,
     * to produce the menu html
     *
     * @param string      $name     MenuBuilder identifier in format BundleName:ClassName:menuFunctionPrefix
     * @param string|bool $template template identifier. Leave false to use default template
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @return string html representing the menu
     */
    public function render($name, $template = false)
    {
        list($bundleName, $className, $menuId) = explode(":", $name);

        $builder    = $this->findMenuClass($bundleName, $className);
        $methodName = $menuId . "Menu";

        if (!method_exists($builder, $methodName)) {
            throw new \InvalidArgumentException("Could not find referenced menu function $methodName");
        }

        $menu = $builder->$methodName();

        Validator::validate($menu);

        $parameters['menu']     = $this->toParameters($menu);
        $parameters['template'] = $template ? : $this->defaultTemplate;

        return $this->twig->render($parameters['template'], $parameters);
    }

    /**
     * Internal function to convert MenuBuilder object into array of parameters which can be passed to a twig template
     * for rendering
     *
     * @param MenuBuilder $mb the menu builder to be converted to parameters
     * @return array representation of menu in an array format
     */
    public function toParameters(MenuBuilder $mb): array
    {
        $menu = ['items' => []];

        if ($mb->getId()) {
            $menu['id'] = $mb->getId();
        }

        $items = $mb->getItems();

        foreach ($items as $item) {
            $sub = [
                'id'      => $item->getId(),
                'caption' => $item->getCaption(),
                'href'    => $this->getHref($item)
            ];

            if ($item->getLinkClass() !== null) {
                $sub['aClass'] = $item->getLinkClass();
            }

            if ($item->hasSubmenu()) {
                $sub['submenu'] = $this->toParameters($item->getSubmenu());

                if(!empty($sub['submenu']['items'])) {
                    $menu['items'][] = $sub;
                }

            } else {
                $menu['items'][] = $sub;
            }
        }

        return $menu;
    }

    /**
     * Internal function for returning href to be used in menu item link.
     *
     * If menu item link is set as route, the href is generated using the router.
     * If menu item link is already href, then it is simply returned unchanged.
     *
     * @param MenuElement $item the item for which generate href
     * @return string generated href
     * @throws \LogicException when neither route nor href link is set on the provided $item
     */
    public function getHref(MenuElement $item): string
    {
        /** for sub items */
        if (null === $item->getRoute()) {
            return '#';
        }

        if ($item->getRoute()) {
            return $this->router->generate($item->getRoute(), $item->getRouteParams());
        }

        throw new \LogicException("Menu item missing both href and route. At least one must be provided");
    }

    /**
     * Attempts to locate menu class based on bundle name and the class name.
     *
     * A menu class is a class containing function(s) which define menus using MenuBuilder.
     * The class must be located in the Menu sub-folder of the bundle.
     *
     * @param $bundleName
     * @param $className
     *
     * @return object located class
     * @throws \InvalidArgumentException if the class was not found
     */
    private function findMenuClass($bundleName, $className)
    {
        $builderId = $bundleName . ':' . $className;

        if (isset($this->builderCache[$builderId])) { //return cached builder
            return $this->builderCache[$builderId];
        }

        $builder       = null;
        $bundle        = $this->kernel->getBundle($bundleName);
        $fullClassName = $bundle->getNamespace() . '\\Menu\\' . $className;

        if (class_exists($fullClassName)) {
            $builder                        = new $fullClassName();
            $this->builderCache[$builderId] = $builder;
        } else {
            //need more research on weather below code could break things.
            $fullClassName = $bundle->getNamespace() . '\\Tests\\Menu\\' . $className;
            if (class_exists($fullClassName)) {
                $builder                        = new $fullClassName();
                $this->builderCache[$builderId] = $builder;
            }
        }

        if ($builder === null) {
            // TODO add proper error description
            throw new \InvalidArgumentException("Could not find menu builder.");
        }

        return $builder;
    }
}
