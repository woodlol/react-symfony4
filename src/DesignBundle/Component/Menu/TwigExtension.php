<?php declare(strict_types=1);

namespace App\DesignBundle\Component\Menu;

use Twig\Extension\AbstractExtension;

/**
 * Defines the twig menu renderer extension functionality.
 */
class TwigExtension extends AbstractExtension
{
    /** @var Renderer  */
    private $renderer;

    /**
     * @param Renderer $renderer
     */
    public function __construct(Renderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Returns array defining all the function provided by this extension so that Twig knows what it can do with this
     * class
     *
     * @return array function definitions
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction("app_menu_render", [$this, 'renderMenu'], ['is_safe' => ['html']])
        );
    }

    /**
     * Function which renders menus inside twig templates
     *
     * @param      $name
     * @param bool $template
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @return string
     */
    public function renderMenu($name, $template = false)
    {
        return $this->renderer->render($name, $template);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return "app_core_menu_extension";
    }
}
