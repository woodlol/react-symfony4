<?php declare(strict_types=1);

namespace App\DesignBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppDesignBundle extends Bundle
{
}
