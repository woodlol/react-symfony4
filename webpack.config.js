const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/css/app.scss')
    .enableSassLoader()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .autoProvidejQuery()
    .configureFilenames({
        js: '[name].[chunkhash].js',
        css: '[name].[contenthash].css',
        images: 'images/[name].[ext]',
        fonts: 'fonts/[name].[hash:8].[ext]'
    })
;

module.exports = Encore.getWebpackConfig();
