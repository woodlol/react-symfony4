<?php declare(strict_types=1);

namespace Tests\CoreBundle\Factory;

use App\CoreBundle\Entity\Dinosaur;
use App\CoreBundle\Factory\DinosaurFactory;
use App\CoreBundle\Components\DinosaurLengthDeterminator;
use PHPUnit\Framework\TestCase;

class DinosaurFactoryTest extends TestCase
{
    /**
     * @var DinosaurFactory
     */
    private $factory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $lengthDeterminator;

    public function setUp(): void
    {
        $this->lengthDeterminator = $this->createMock(DinosaurLengthDeterminator::class);
        $this->factory            = new DinosaurFactory($this->lengthDeterminator);
    }

    public function testItGrowsALargeVelociraptor(): void
    {
        $dinosaur = $this->factory->growVelociraptor(5);

        $this->assertInstanceOf(Dinosaur::class, $dinosaur);
        $this->assertIsString($dinosaur->getGenus());
        $this->assertSame('Velociraptor', $dinosaur->getGenus());
        $this->assertSame(5, $dinosaur->getLength());
    }

    public function testItGrowsABabyVelociraptor(): void
    {
        $dinosaur = $this->factory->growVelociraptor(1);

        $this->assertSame(1, $dinosaur->getLength());
    }

    /**
     * @dataProvider getSpecificationTests
     *
     * @param string $spec
     * @param bool   $expectedIsCarnivorous
     *
     * @throws \Exception
     */
    public function testItGrowsADinosaurFromSpecification(string $spec, bool $expectedIsCarnivorous): void
    {
        $this->lengthDeterminator->expects($this->once())->method('getLengthFromSpecification')->with($spec)->willReturn(20);

        $dinosaur = $this->factory->growFromSpecification($spec);

        $this->assertSame($expectedIsCarnivorous, $dinosaur->isCarnivorous(), 'Diets do not match');
        $this->assertSame(20, $dinosaur->getLength());
    }

    /**
     * @return array
     */
    public function getSpecificationTests(): array
    {
        return [
            // specification, is large, is carnivorous
            ['large carnivorous dinosaur', true],
            'default response' => ['give me all the cookies!!!', false],
            ['large herbivore', false],
        ];
    }
}
