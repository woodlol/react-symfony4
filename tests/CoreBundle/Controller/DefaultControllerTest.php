<?php declare(strict_types=1);

namespace Tests\CoreBundle\Controller;

use App\CoreBundle\DataFixtures\LoadBasicParkData;
use App\CoreBundle\DataFixtures\LoadSecurityData;
use App\CoreBundle\DataFixtures\UserFixtures;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase
{
    /** @var Client */
    protected static $client;

    /** @var ReferenceRepository */
    private $fixtures;

    protected function setUp(): void
    {
        $this->fixtures = $this->loadFixtures([
            UserFixtures::class,
            LoadBasicParkData::class,
            LoadSecurityData::class,
        ])->getReferenceRepository();

        self::$client = $this->makeClient(true);

    }

    public function testDefault(): void
    {
        self::$client->request(Request::METHOD_POST, '/security/login');

        $this->assertStatusCode(Response::HTTP_FOUND, self::$client);
    }

//    public function testEnclosuresAreShownOnHomepage()
//    {
//        $crawler = self::$client->request(Request::METHOD_GET, '/default/main');
//        self::$client->followRedirect();
//        $this->assertStatusCode(Response::HTTP_OK, self::$client);
//
//        $table = $crawler->filter('.table-enclosures');
//        $this->assertCount(3, $table->filter('tbody tr'));
//    }
//
//    public function testThatThereIsAnAlarmButtonWithoutSecurity()
//    {
//        $crawler = self::$client->request(Request::METHOD_GET, '/');
//
//        $enclosure = $this->fixtures->getReference('carnivorous-enclosure');
//        $selector  = sprintf('#enclosure-%s .button-alarm', $enclosure->getId());
//
//        $this->assertGreaterThan(0, $crawler->filter($selector)->count());
//    }
//
//    public function testItGrowsADinosaurFromSpecification()
//    {
//        self::$client->followRedirects();
//
//        $crawler = self::$client->request(Request::METHOD_GET, '/');
//
//        $this->assertStatusCode(Response::HTTP_OK, self::$client);
//
//        $form = $crawler->selectButton('Grow dinosaur')->form();
//        $form['enclosure']->select(3);
//        $form['specification']->setValue('large herbivore');
//
//        self::$client->submit($form);
//        $this->assertContains('Grew a large herbivore in enclosure #3', self::$client->getResponse()->getContent());
//    }
}
