<?php declare(strict_types=1);

namespace CoreBundle\Components;

use App\CoreBundle\Components\Common;
use PHPUnit\Framework\TestCase;

class CommonTest extends TestCase
{
    public function testMethodFormComponent(): void
    {
        $component = new Common();

        $this->assertNotNull($component->getRandValue());
    }
}
