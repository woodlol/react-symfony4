<?php declare(strict_types=1);

namespace CoreBundle\Components;

use App\CoreBundle\Components\MarkdownHelper;
use Michelf\MarkdownInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class MarkdownHelperTest extends TestCase
{
    /** @var AdapterInterface  */
    private $cache;

    /** @var MarkdownInterface  */
    private $markdown;

    /** @var LoggerInterface  */
    private $logger;

    /** @var bool  */
    private $isDebug;

    protected function setUp(): void
    {
        $this->cache    = $this->createMock(AdapterInterface::class);
        $this->markdown = $this->createMock(MarkdownInterface::class);
        $this->logger   = $this->createMock(LoggerInterface::class);
        $this->isDebug  = true;
    }

    /**
     * @dataProvider provideItemStrings
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function testComponent(string $str): void
    {
        $component = new MarkdownHelper(
            $this->cache,
            $this->markdown,
            $this->logger,
            $this->isDebug
        );

        $this->assertNotNull($component->parse($str));
    }

    /**
     * @return \Generator
     */
    public function provideItemStrings(): \Generator
    {
        for ($i = 0; $i < 10; ++$i) {
            yield['bacon'];
        }
    }
}
