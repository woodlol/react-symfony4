<?php declare(strict_types=1);

namespace Tests\CoreBundle\Command;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class SoundAlarmCommandTest extends WebTestCase
{
    public function testItOutputsInUppercaseOnYellOption(): void
    {
        $run = true;

        try {
            $this->runCommand('app:sound-alarm', [
                'message' => 'run',
                '--yell' => true,
            ]);
        } catch (\Throwable $exception) {
            $run = false;
        }

        $this->assertTrue($run);
    }
}
