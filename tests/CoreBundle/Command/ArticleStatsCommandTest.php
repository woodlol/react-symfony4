<?php declare(strict_types=1);

namespace CoreBundle\Command;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ArticleStatsCommandTest extends WebTestCase
{
    public function testCommand()
    {
        $run = true;

        try {
            $this->runCommand('article:stats', [
                'slug'     => 'test',
                '--format' => \rand(0, 1) ? 'json' : 'text',
            ]);
        } catch (\Throwable $exception) {
            $run = false;
        }

        $this->assertTrue($run);
    }
}
