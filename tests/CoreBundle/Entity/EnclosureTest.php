<?php declare(strict_types=1);

namespace Tests\CoreBundle\Entity;

use App\CoreBundle\Entity\Dinosaur;
use App\CoreBundle\Entity\Enclosure;
use App\CoreBundle\Exception\DinosaursAreRunningRampantException;
use App\CoreBundle\Exception\NotABuffetException;
use PHPUnit\Framework\TestCase;

class EnclosureTest extends TestCase
{
    public function testItHasNoDinosaursByDefault(): void
    {
        $enclosure = new Enclosure();

        $this->assertEmpty($enclosure->getDinosaurs());
    }

    public function testItAddsDinosaurs(): void
    {
        $enclosure = new Enclosure(true);

        $enclosure->addDinosaur(new Dinosaur());
        $enclosure->addDinosaur(new Dinosaur());

        $this->assertCount(2, $enclosure->getDinosaurs());
    }

    public function testItDoesNotAllowCarnivorousDinosToMixWithHerbivores(): void
    {
        $enclosure = new Enclosure(true);

        $enclosure->addDinosaur(new Dinosaur());

        $this->expectException(NotABuffetException::class);

        $enclosure->addDinosaur(new Dinosaur('Velociraptor', true));
    }

    public function testItDoesNotAllowToAddNonCarnivorousDinosaursToCarnivorousEnclosure(): void
    {
        $assert = false;

        try {
            $enclosure = new Enclosure(true);

            $enclosure->addDinosaur(new Dinosaur('Velociraptor', true));
            $enclosure->addDinosaur(new Dinosaur());
        } catch (NotABuffetException $exception) {
            if ($exception instanceof NotABuffetException) {
                $assert = true;
            }
        }

        $this->assertTrue($assert);

    }

    public function testItDoesNotAllowToAddDinosToUnsecureEnclosures(): void
    {
        $enclosure = new Enclosure();

        $this->expectException(DinosaursAreRunningRampantException::class);
        $this->expectExceptionMessage('Are you craaazy?!?');

        $enclosure->addDinosaur(new Dinosaur());
    }
}
