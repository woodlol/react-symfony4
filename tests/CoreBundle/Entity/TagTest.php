<?php declare(strict_types=1);

namespace CoreBundle\Entity;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Tag;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase
{
    public function testCreateAndAddToArticleTag(): void
    {
        $tag     = new Tag();
        $article = new Article();

        $article->addTag($tag);

        $this->assertEquals(1, $article->getTags()->count());

        $article->removeTag($tag);

        $this->assertEquals(0, $article->getTags()->count());
    }
}
