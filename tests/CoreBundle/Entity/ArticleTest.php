<?php declare(strict_types=1);

namespace CoreBundle\Entity;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Comment;
use App\CoreBundle\Entity\Tag;
use App\CoreBundle\Entity\User;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    public function testCreateArticleAndSetSomeComment()
    {
        $article = new Article();
        $comment = Comment::create(User::DEFAULT_ROOT_NAME, 'test');
        $tag     = (new Tag())->setName('Tag');

        $article
            ->setAuthor(User::DEFAULT_ROOT_NAME)
            ->setTitle('Title')
            ->addComment($comment)
            ->addTag($tag);

        $this->assertEquals(User::DEFAULT_ROOT_NAME, $article->getAuthor());

        $this->checkNumberOfProperties($article, 1);

        $article
            ->removeComment($comment)
            ->removeTag($tag);

        $this->checkNumberOfProperties($article, 0);
    }

    /**
     * @param Article $article
     * @param int     $expected
     */
    private function checkNumberOfProperties(Article $article, int $expected): void
    {
        $this->assertEquals($expected, $article->getComments()->count());
        $this->assertEquals($expected, $article->getTags()->count());
    }
}
