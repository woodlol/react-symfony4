<?php declare(strict_types=1);

namespace CoreBundle\Entity;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Comment;
use App\CoreBundle\Entity\User;
use PHPUnit\Framework\TestCase;

class CommentTest extends TestCase
{
    public function testCreateCommentAndAddToArticle()
    {
        $comment = Comment::create(User::DEFAULT_ROOT_NAME, 'asdasasda');

        $article = new Article();
        $article->addComment($comment);

        $this->assertSame(User::DEFAULT_ROOT_NAME, $article->getComments()->first()->getAuthorName());
        $this->assertNotNull($comment->getArticle());
    }
}
