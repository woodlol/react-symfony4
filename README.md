# My awensome project on symfony 4 with something what i know.

Project for study and deepen knowledge.

## Status
[![build status](https://gitlab.com/woodlol/react-symfony4/badges/develop/build.svg)](https://gitlab.com/woodlol/react-symfony4/commits/develop)
[![coverage report](https://gitlab.com/woodlol/react-symfony4/badges/develop/coverage.svg)](https://gitlab.com/woodlol/react-symfony4/commits/develop)

Helpful information
-------------------
 * **Production** -- master
 * **Test for new features** -- develop

 
It comes with the following bundles.
------------------------------------
_See documentations bellow:_
  * [**FrameworkBundle**][1] - The core Symfony framework bundle
  * [**DoctrineBundle**][2] - Adds support for the Doctrine ORM
  * [**DoctrineMigrationsBundle**][3] - Adds support Doctrine Migrations
  * [**DoctrineFixturesBundle**][4] - Adds support Doctrine Fixtures 
  * [**TwigBundle**][5] - Adds support for the Twig template
  * [**TwigExtensions**][6] - Adds support for the Twig Extensions
  * [**Webpack Encore**][7] - Managing CSS and JavaScript
Enjoy!

[1]:  https://symfony.com/doc/current/index.html
[2]:  http://docs.doctrine-project.org/
[3]:  http://docs.doctrine-project.org/projects/doctrine-migrations/en/latest/reference/introduction.html
[4]:  http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
[5]:  http://twig.sensiolabs.org/documentation
[6]:  http://twig.sensiolabs.org/doc/extensions/index.html
[7]:  https://symfony.com/doc/current/frontend.html
